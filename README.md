# TOLOlib

**Python Library and Command Line Interface for Communicating with TOLO Steam Generators**

![](https://img.shields.io/pypi/pyversions/tololib)
![](https://img.shields.io/pypi/dm/tololib)
![](https://img.shields.io/pypi/l/tololib)
![](https://gitlab.com/MatthiasLohr/tololib/badges/main/pipeline.svg)
![](https://gitlab.com/MatthiasLohr/tololib/badges/main/coverage.svg)

This library supports controlling and requesting information frmo TOLO Sauna/Steam Bath systems.
It is the core of the [Home Assistant TOLO Integration](https://www.home-assistant.io/integrations/tolo/).
If you are missing any features or you found any bugs or irregularities,
please do not hesitate to [open an issue](https://gitlab.com/MatthiasLohr/tololib/-/issues/new).


## Links

  * [API Documentation](https://matthiaslohr.gitlab.io/tololib/)
  * [Official Website of Steamtec/TOLO](https://www.tolosauna.com/)


## Legal Notice

This project is a community project, published under the [MIT License](LICENSE.md).
See [LICENSE.md](https://gitlab.com/MatthiasLohr/tololib/-/blob/main/LICENSE.md) for more information.

Neither this project, nor the author is in any professional affiliation with companies behind TOLO.

Copyright (c) 2021-2025 by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
